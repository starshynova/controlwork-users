#!/bin/bash
groupadd netadmin
useradd -m lucy -G netadmin
passwd lucy
passwd robert

vi /etc/sudoers
#Allow people from group netadmin to run tcpdump
#%netadmin	ALL=(ALL)	/usr/sbin/tcpdump

dnf install tcpdump -y
su lucy
cd
sudo tcpdump -i ens18
